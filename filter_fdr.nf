#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process FILTER_FDR {
	stageInMode "copy"

	input:
	path meta
	path(pep)
	path(prot)
	val params
	path group_dir

	output:
	tuple path('.meta'), path(group_dir)

	shell:
	'''
	mkdir group
	MZML="$(find !{group_dir} -name "*.mzML")"
	for m in $MZML
	do
		base_m="$(basename $m)"
		m_name="${base_m%.*}"
		cp "interact-${m_name}.pep.xml" group/
	done
	ppp --path "$PWD" "!{meta}/meta.bin"
	philosopher filter !{params} --pepxml ./group --protxml "!{prot}"
	'''
}
