#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process BUILD_FASTA {
	debug true
	stageInMode "copy"

	input:
	path meta
	val id
	path custom_fasta

	output:
	path '*.fas', emit: db
	path '.meta', emit: meta

	shell:
	'''
	ppp --path "$PWD" "!{meta}/meta.bin"

	philosopher database --id !{id} --contam --reviewed
	'''
}
