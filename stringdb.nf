#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process STRINGDB {
    debug true
	publishDir params.output_dir, mode: 'copy'
    
	input:
	val species_id
    path msstats_out

    output:
    path 'stringdb/', emit: stringdb_out

	shell:
	'''
	mkdir stringdb
	stringDB --species_id "!{species_id}" --input_file "!{msstats_out}/PairwiseComparison.csv" --output_dir stringdb
	'''
}

