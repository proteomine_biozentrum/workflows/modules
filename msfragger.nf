#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process MSFRAGGER {
    stageInMode "copy"	

	input:
	path db
	path input_dir
	val ram
	val param_type
	val params_cmd

	output:
	path('**/*.pepXML'), emit: pepXML

	shell:
	'''
	msfragger --config !{param_type}
	sed -i -e 's/database_name.*/database_name = !{db}/' !{param_type}_fragger.params

	echo "!{params_cmd}" > cmd
	chmod +x cmd
	./cmd
	
	JAVA_OPTS="-Xmx!{ram}" msfragger !{param_type}_fragger.params "!{input_dir}/**/*.mzML"
	'''
}

