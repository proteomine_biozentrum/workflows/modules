#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process PEPTIDE_PROPHET {
	stageInMode "copy"

	input:
	path meta
	path db
	path(pep)
	val params

	output:
	path('interact*.pep.xml'), emit: pep_xml
	path '.meta', emit: meta

	shell:
	'''
	ppp --path "$PWD" "!{meta}/meta.bin"
	philosopher peptideprophet --database "!{db}" !{params} !{pep}
	'''
}
