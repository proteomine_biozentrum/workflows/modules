#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process LABELQUANT {
	stageInMode "copy"

	input:
	tuple path(meta), path(group_dir)
	path pep
	path prots
	val tmt

	output:
	tuple path('.meta'), path(group_dir)

	shell:
	'''
	cp "!{workflow.projectDir}/tmt_!{tmt}_annotation.txt" annotation.txt

	ppp --path "$PWD" "!{meta}/meta.bin"
	philosopher labelquant --brand tmt --plex !{tmt} --dir "!{group_dir}"
	'''
}
