#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process FLAG_CUSTOMS {
	publishDir params.output_dir, mode: 'copy'

	input:
	path custom_fasta
	path report_dir

	output:
	path report_dir, emit: custom_flags

	shell:
	'''
	flag_proteins --protein "!{report_dir}/protein.tsv" --fasta_file "!{custom_fasta}" --output_dir "!{report_dir}"
	'''
}

