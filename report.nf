#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process REPORT {
	stageInMode "copy"
	publishDir params.output_dir, pattern: "${group_name}_report", mode: "copy"

	input:
	tuple path(meta), path(group_name)
	path(pep)
	path(prots)

	output:
	path "${group_name}_report", emit: report_dir
	path group_name, emit: spec_dir

	shell:
	'''
	ppp --path "$PWD" "!{meta}/meta.bin"
	philosopher report

	mkdir "!{group_name}_report"
	mv *.tsv "!{group_name}_report"
	mv protein.fas "!{group_name}_report"
	'''
}
